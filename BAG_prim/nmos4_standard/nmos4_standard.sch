v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -70 -0 -40 -0 {
lab=G}
N 0 -60 0 -30 {
lab=D}
N -70 -60 0 -60 {
lab=D}
N 0 30 -0 60 {
lab=S}
N -70 60 -0 60 {
lab=S}
N 0 -0 60 0 {
lab=B}
C {devices/iopin.sym} -70 -60 0 1 {name=p1 lab=D}
C {devices/iopin.sym} -70 0 0 1 {name=p2 lab=G}
C {devices/iopin.sym} -70 60 0 1 {name=p3 lab=S}
C {devices/iopin.sym} 60 0 0 0 {name=p4 lab=B}
C {sky130_fd_pr/nfet_01v8.sym} -20 0 0 0 {name=M1
L=l
W=1
nf=nf
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
