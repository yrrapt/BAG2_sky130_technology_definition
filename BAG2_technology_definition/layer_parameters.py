"""
======
BAG2 layer definition module
======

The layer definition module of BAG2 framework. Currently collects all definition not
directly related to 

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 2020.

The goal of the class is to provide a master source for the process information. 
Currently it inherits TechInfoConfig, which in turn uses the values defined in it.
This circularity should be eventually broken.

Documentation instructions
--------------------------

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""
import os
import pkg_resources
import yaml
from BAG2_technology_template.layer_parameters_template import layer_parameters_template

class layer_parameters(layer_parameters_template):

    @property
    def tech_lib(self):
        ''' Name of the process technology library containing transistor primitives. 
        
        ''' 
        return 'skywater130'  # TODO: what to put here?

    @property
    def layout_unit(self):
        '''Layout unit, in meters

        ''' 
        return 1.0e-6 

    @property
    def resolution(self):
        '''Layout resolution in layout units.
        
        ''' 
        return 0.005

    @property
    def flip_parity(self):
        '''True if this is multipatterning technology. 

        type: Boolean
        
        [TODO] Define where used and for what.
        There is a method 'use_flip_parity' for this name in 
        TechInfoConfig, so therefore we can not 
        define the property with a same name.
        
        ''' 
        return False

    @property
    def pin_purpose(self):
        """Purpose of the pin layer used in pins constructs
            
              str : Default 'pin'

        """
        return 'pin'



    @property
    def well_layers(self):
        '''Dictionary of tuples of form 
        key:  (layer,purpose)
        describing the layers used for ntap and ptap.

        Current keys 
        ------------ 
                ntap : (str,str) 
                ptap : (str,str)

        '''
        return { 'ntap' : ('nwell', 'drawing'),
                 'ptap' : []
                }

    @property
    def mos_layer_table(self):
        '''Dictionary of tuples of form 
        key:  (layer,purpose)
        describing the layers used for ntap and ptap.

        Current keys 
        ------------ 
                PO : (str,str), Poly layer 
                PO_sub : (str,str), Substrate Poly layer 
                PO__gate_dummy : (str,str), Dummy Poly on dummy OD
                PO_dummy : (str,str), Dummy Poly not on any OD
                PO_edge : (str,str), Edge Poly
                PO_edge_sub : (str,str), Edge Poly on substrate OD
                PO_edge_dummy : (str,str), Edge Poly on dummy OD
                PODE : None, Poly on OD edge layer

                OD : (str,str), Active layer
                OD_sub: (str,str), Substrate active layer
                OD_dummy: (str,str), Dummy active layer
                
                MP: (str,str), Gate connection metal
                MD: (str,str), OD connection metal
                MD_dummy: (str,str), dummy OD connection metal

                CPO: (str,str), cut poly
                FB: (str, str), fin boundary layer

        '''
        return { 'PO' : ('poly', 'drawing'),
                 'PO_dummy' : ('poly', 'drawing'),  # TODO: no dummy purpose provided currently - how to avoid dummies at all?
                 'OD' : ('diff', 'drawing'),
                 'OD_dummy' : ('diff', 'drawing'),  # TODO: no dummy purpose provided currently - how to avoid dummies at all?
                }

    @property
    def res_layer_table(self):
        '''Dictionary of tuples of form 
        key:  (layer,purpose)
        describing the layers used for a metal resistors.

        Current keys 
        ------------ 
                RPDMY : (str,str), a layer drawn exactly on top of metal resistor,
                        generally used for LVS recognition purposes.

                RPO : (str,str), the "resistive poly" layer that makes a poly more resistive.
 
        '''
        # TODO: is that applying to poly resistors only? Description says "metal" ...
        return { 'RPDMY' : ('poly', 'resistor'),  # TODO: not clear what to use here for sky 130
                 'RPO' : ('poly', 'resistor'),    # TODO: translation to sky 130?
                }

    @property
    def res_metal_layer_table(self):
        ''' Mapping from metal layer ID to layer/purpose pair that defines
        a metal resistor.
        
        Array of tuples of form (layer,purpose)
        INdex 0 is usually (None,None) as there ins no Metal0

        '''
        return { 
                  1 : ('li1', 'res'),
                  2 : ('met1', 'res'),
                  3 : ('met2', 'res'),
                  4 : ('met3', 'res'),
                  5 : ('met4', 'res'),
                  6 : ('met5', 'res'),
                }

    @property
    def metal_exclude_table(self):
        '''Mapping from metal layer ID to layer/purpose pair that
        defines metal exclusion region.
        
        Dict of tuples of form int : (layer,purpose)


        '''
        # TODO: what is the meaning of these layers? And what layers to use in sky 130?
        return { 
                1 : ('li1', 'blockage'),        
                2 : ('met1', 'blockage'),
                3 : ('met2', 'blockage'),
                4 : ('met3', 'blockage'),
                5 : ('met4', 'blockage'),
                6 : ('met5', 'blockage'),
                }

    @property
    def layer_name(self):
        '''
        Mapping dictionary from metal layer ID to layer name. Assume purpose is 'drawing'.

        Dict of of form int : 'layer'
        
        '''
        return { 
                1 : 'li1',
                2 : 'met1',
                3 : 'met2',
                4 : 'met3',
                5 : 'met4',
                6 : 'met5',
                }

    @property
    def layer_type(self):
        '''Mapping from metal layer name to metal layer type.  The layer type
         is used to figure out which EM rules to use. OD has no current density
         EM rules, but layer type is used for via mapping as well. Hence, it
         is also here.

        Dict of of form int : 'layer'
        
        '''
        return { 
                'li1' : '1',    # w=0.17
                'met1' : '2',   # w=0.14
                'met2' : '2',   # w=0.14
                'met3' : '3',   # w=0.3
                'met4' : '3',   # w=0.3
                'met5' : '4',   # w=1.6
                }

    @property
    def max_w(self):
        ''' Maximum wire width for given metal type
        
        '''
        # TODO: what to put here?
        return { 
            '1': int ,
            '2': int ,
            '3': int ,
            '4': int ,
        }

    @property
    def min_area(self):
        ''' Minimum area for given metal type
        
        '''
        # TODO: what to put here?
        return { 
                '1': [int ] ,
                '2': [int ] ,
                '3': [int ] ,
                '4': [int ] ,
               }

    @property
    def sp_le_min(self):
        ''' Minimum line-end spacing rule. Space is measured parallel to wire direction
        '''

        return { 
            '1': {
                'w_list' : [float('Inf')],
                'sp_list': [34],
            },
            '2' : {
                'w_list' : [float('Inf')],
                'sp_list' : [31],
            },
            '3' : {
                'w_list' : [float('Inf')],
                'sp_list' : [60],
            },
            '4' : {
                'w_list' : [float('Inf')],
                'sp_list' : [320],
            },
        }


    @property
    def len_min(self):
        ''' Minimum length/minimum area rules.

        '''
        # @@@ TODO: no idea what this list is about ...
        return {
                '1' : {  
                    'w_list' :  [float('Inf')],
                    'w_al_list' :  [[0 , 0 ]],
                    'md_list' :  [],
                    'md_al_list' :  [],
                    },
                '2' : {
                  'w_list' :  [float('Inf')],
                  'w_al_list' :  [[2704 , 0 ]],
                  'md_list' :  [],
                  'md_al_list' :  [],
                  },
                '3' : { 
                  'w_list' :  [float('Inf')],
                  'w_al_list' :  [[9600 , 0 ]],
                  'md_list' :  [],
                  'md_al_list' :  [],
                  },
                '4' : { 
                  'w_list' :  [float('Inf')],
                  'w_al_list' :  [[160000 , 0 ]],
                  'md_list' :  [],
                  'md_al_list' :  [],
                  },
                }

    @property
    def idc_em_scale(self):
        '''Table of electromigration temperature scale factor
        '''
        return {
        # scale factor for resistor
        # scale[idx] is used if temperature is less than or equal to temp[idx]
            'res' : {
                'temp' : [100 , float('Inf') ],
                'scale' : [1.0, 0.5]
                # scale factor for this metal layer type
            },
            # TODO: is this per type?
            '1' : {
                'temp' : [100 , float('Inf') ],
                'scale' : [1.0, 0.5]
            },
            '2' : {
                'temp' : [100 , float('Inf') ],
                'scale' : [1.0, 0.5]
            },
            '3' : {
                'temp' : [100 , float('Inf') ],
                'scale' : [1.0, 0.5]
            },
            '4' : {
                'temp' : [100 , float('Inf') ],
                'scale' : [1.0, 0.5]
            },
            # default scale vector
            'default' : {
                'temp' : [100 , float('Inf') ],
                'scale' : [1.0, 0.5]
            }
        }

    @property 
    def sp_min(self):
        ''' Minimum wire spacing rule.  Space is measured orthogonal to wire direction.
        Wire spacing as function of wire width.  sp_list[idx] is used if wire width is 
        less than or equal to w_list[idx].

        '''
        return { 
    # wire spacing as function of wire width.  sp_list[idx] is used if
    # wire width is less than or equal to w_list[idx]
            '1': {
                'l_list' : [float('Inf')],
                'w_list' : [float('Inf')],
                'sp_list': [34],
            },
            '2' : {
                'l_list' : [float('Inf')],
                'w_list' : [float('Inf')],
                'sp_list' : [31],
            },
            '3' : {
                'l_list' : [float('Inf')],
                'w_list' : [float('Inf')],
                'sp_list' : [60],
            },
            '4' : {
                'l_list' : [float('Inf')],
                'w_list' : [float('Inf')],
                'sp_list' : [320],
            },
          }

    @property 
    def sp_sc_min(self):
        ''' Minimum wire spacing rule for same color wires, FINFET specific. 

        '''
        return { 
            '1x': {
              'l_list' : [float('Inf')],
              'w_list' : [int, float('Inf')],
              'sp_list' : [int],
              },
            '4' : {
              'l_list' :   [float('Inf')],
              'w_list' :   [int, float('Inf')],
              'sp_list' :  [int ],
              },
            '2x' : {
              'l_list' :  [float('Inf')],
              'w_list' :  [int , float('Inf')],
              'sp_list' :  [int ],
              },
            '4x' : {
              'l_list' :  [float('Inf')],
              'w_list' :  [int , float('Inf')],
              'sp_list' :  [int],
              },
            't' : { 
              'l_list' :  [float('Inf')],
              'w_list' :  [int, float('Inf')],
              'sp_list' :  [int ],
              }
            }

    @property
    def dnw_margins(self):
        ''' Deep Nwell margins

        '''
        # TODO: review these values, whatever this means ...
        return {
            'normal' :    250 ,
            'adjacent' :  250 ,
            'compact' :   250 
        }
#            'dnw_dnw_sp' :  int , # Deep N-well to Deep N-well with different potential space
#            'dnw_nw_sp' :  int , # Deep N-well to N-well with different potential space
#            'dnw_pw_sp' :  int ,
#            'dnw_np_enc' :  int , # Minimum Deep N-well enclosure of NP
#           }

    @property
    def implant_rules(self): 
        '''' Implant layer rules.

        '''
        return {
            'imp_ext_on_act' :  int , # Minimum implantation extension from active OD
            'imp_ext_on_strap' :  int , # Minimum implantation extension from active OD in N-well
            'imp_min_w' :  int , # Minimum implant space
            'imp_min_sp' : {  # Minimum implant to implant space (function of implant spacing)'' :  
                'imp_w' :  [int , float('Inf')], 
                'imp_sp' :  [int , int ],
            }
        }
    @property
    def nw_rules(self): 
        ''' N-well layer rules.

        '''
        return {
             'nw_min_w' :  int , # Minimum N-well width
             'nw_min_sp' :  int , # Minimum space for int N-well with same potential
             'nw_core_sp' :  int , # Minimum space for int N-well with diff. potential (core device)'' : 
             'nw_io_core_sp' :  int , # Minimum between I/O device N-well and core device N-well with diff. pot.
             'nw_io_sp' :  int , # Minimum space for int N-well with diff. potential (I/O device)'' : 
             'nw_enc_on_strap' :  int ,
        }

    @property
    def od_rules(self):
        ''' OD layer rules

        '''
        return {
            'od_min_w' :  int , # Minimum width of OD
            'od_min_sp' :  {# Minimum spacing of two OD's (function of width)
                'od_w' :  [int , float('Inf')],
                'od_sp' :  [int , int ],
           }
        }

    @property
    def property_dict(self):
        '''Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        '''
        if not hasattr(self, '_property_dict'):
           # yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
           # with open(yaml_file, 'r') as content:
           #   #self._process_config = {}
           #    dictionary = yaml.load(content, Loader=yaml.FullLoader )

           # self._property_dict = dictionary
            self._property_dict = {}

            for key, val  in vars(type(self)).items():
                if isinstance(val,property) and key != 'property_dict':
                    if key == 'flip_parity':
                        self._property_dict['use_flip_parity']=getattr(self,key)
                    else:
                        self._property_dict[key] = getattr(self,key)
        return self._property_dict

